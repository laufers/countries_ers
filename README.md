### Repository base for Git and GitHub Instruction

Credit for this goes to [Jonah Duckles](https://github.com/jduckles)

This repository serves as the base repository for use in a Software Carpentry Workshop 
to show an example workflow for  collaborating on a project. The lesson plan is under
development and will be linked when complete. 


####Use

Import, **fork**, this repository to your host account on Bitbucket. 


####Disclaimer

This is a work in progress
